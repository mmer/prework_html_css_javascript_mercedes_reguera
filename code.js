function verExperiencia() {
  var boton = document.getElementById('button-experience');
  boton.innerHTML="Ver experiencia";

  var element = document.getElementById('experience');
  element.classList.toggle("active");

  if (element.classList.contains("active")){
    boton.innerHTML="Ocultar experiencia";
  }
}


function verEstudios() {
  var boton = document.getElementById('button-studies');
  boton.innerHTML="Ver estudios";

  var element = document.getElementById('studies');
  element.classList.toggle("active");

  if (element.classList.contains("active")){
    boton.innerHTML="Ocultar estudios";
  }
}

function verIdiomas() {
  var boton = document.getElementById('button-idiomas');
  boton.innerHTML="Ver idiomas";

  var element = document.getElementById('idiomas');
  element.classList.toggle("active");

  if (element.classList.contains("active")){
    boton.innerHTML="Ocultar idiomas";
  }
}

function imprimirNombre() {
  var nombre = document.getElementById('name');
  
  for (var i = 0; i < 100; i++) {
    console.log(nombre.innerText);
  }
}
